package com.example.LazerProj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LazerProjApplication {

    public static void main(String[] args) {
        SpringApplication.run(LazerProjApplication.class, args);
    }

}
