package com.example.LazerProj.Task1;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class Person {

    private final Gun lazer;

    public void shoot() {
        lazer.shoot();
    }
}
