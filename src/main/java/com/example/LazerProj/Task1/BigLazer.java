package com.example.LazerProj.Task1;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@Primary
public class BigLazer implements Gun {

    @Override
    public void shoot() {
        log.info("Wab ! WAB ! ");

    }
}
