package com.example.LazerProj.Task1;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Slf4j
@Component
public class Lazer implements Gun {


    @Value("${lazer.shoot.sound:Pew Pew}")
    private String shootingSound;

    public void shoot() {
        log.info(shootingSound);
    }
}
