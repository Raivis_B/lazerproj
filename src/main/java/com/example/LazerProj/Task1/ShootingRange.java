package com.example.LazerProj.Task1;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;


@Slf4j
@Service
@RequiredArgsConstructor
public class ShootingRange implements CommandLineRunner {

    private final Person person;
    private final Enemy enemy;


    @Override
    public void run(String... args) throws Exception {
        person.shoot();
        enemy.shoot();
    }
}

