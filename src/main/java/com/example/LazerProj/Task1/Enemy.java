package com.example.LazerProj.Task1;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component

public class Enemy implements Gun {
    public Enemy(@Qualifier("lazer") Gun weapon) {
        this.weapon = weapon;
    }

    private final Gun weapon;

    public void shoot() {
        weapon.shoot();
    }
}
